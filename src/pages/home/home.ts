import { Component } from '@angular/core';
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public afService:any;
  items: FirebaseListObservable<any[]>;

  constructor(public navCtrl: NavController, af: AngularFire) {
    this.afService = af;
    this.items = this.initItems(50);
  }

  initItems(qty, serchQuery="guttenavan") {
    return this.afService.database.list('/', {
              query: {
                limitToFirst: qty,
                orderByChild: 'word',
                equalTo: serchQuery
              }
            })
  }

   getItems(ev) {

    this.initItems(50);

    var val = ev.target.value;

    if (val && val.trim() != '') {
      this.items = this.initItems(9999, val.toLowerCase()); // guttenavan, hade, hales, guide,
      console.log(val.toLowerCase());
    }
  }

}
